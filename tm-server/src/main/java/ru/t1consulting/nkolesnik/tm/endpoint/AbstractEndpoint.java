package ru.t1consulting.nkolesnik.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.service.IServiceLocator;
import ru.t1consulting.nkolesnik.tm.dto.request.AbstractUserRequest;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.exception.entity.AccessDeniedException;
import ru.t1consulting.nkolesnik.tm.model.User;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    @Getter
    @Nullable
    protected IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void check(@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        if(request==null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new AccessDeniedException();
        @Nullable final Role userRole = user.getRole();
        if (userRole != role) throw new AccessDeniedException();
    }

    protected void check(@Nullable final AbstractUserRequest request) {
        if(request==null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
    }

}
