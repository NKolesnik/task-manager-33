package ru.t1consulting.nkolesnik.tm.api.service;

public interface IDomainService {

    void loadDataBackup();

    void saveDataBackup();

    void loadDataBase64();

    void saveDataBase64();

    void loadDataBinary();

    void saveDataBinary();

    void loadDataJsonFasterXml();

    void saveDataJsonFasterXml();

    void loadDataJsonJaxb();

    void saveDataJsonJaxb();

    void loadDataXmlFasterXml();

    void saveDataXmlFasterXml();

    void loadDataXmlJaxb();

    void saveDataXmlJaxb();

    void loadDataYamlFasterXml();

    void saveDataYamlFasterXml();

}
