package ru.t1consulting.nkolesnik.tm.dto.response.task;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.response.AbstractResponse;
import ru.t1consulting.nkolesnik.tm.model.Task;

@Getter
public abstract class AbstractTaskResponse extends AbstractResponse {

    @Nullable
    private Task task;

    public AbstractTaskResponse(@Nullable final Task task) {
        this.task = task;
    }

}

