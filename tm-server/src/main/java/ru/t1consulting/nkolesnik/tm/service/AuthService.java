package ru.t1consulting.nkolesnik.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.service.IAuthService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.api.service.IUserService;
import ru.t1consulting.nkolesnik.tm.exception.entity.AccessDeniedException;
import ru.t1consulting.nkolesnik.tm.exception.field.LoginEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.PasswordEmptyException;
import ru.t1consulting.nkolesnik.tm.model.User;
import ru.t1consulting.nkolesnik.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    public AuthService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserService userService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

    @Override
    public User check(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = userService.findByLogin(login);
        if (user.getLocked()) throw new AccessDeniedException();
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @NotNull final String hash = HashUtil.salt(password, secret, iteration);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        return user;
    }

}
