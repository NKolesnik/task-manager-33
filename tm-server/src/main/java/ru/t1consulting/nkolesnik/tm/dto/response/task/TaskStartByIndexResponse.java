package ru.t1consulting.nkolesnik.tm.dto.response.task;

import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.model.Task;

public class TaskStartByIndexResponse extends AbstractTaskResponse {

    public TaskStartByIndexResponse(@Nullable Task task) {
        super(task);
    }

}
