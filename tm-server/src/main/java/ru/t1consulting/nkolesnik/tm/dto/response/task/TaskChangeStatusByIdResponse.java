package ru.t1consulting.nkolesnik.tm.dto.response.task;

import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.model.Task;

public class TaskChangeStatusByIdResponse extends AbstractTaskResponse {

    public TaskChangeStatusByIdResponse(@Nullable final Task task) {
        super(task);
    }

}
