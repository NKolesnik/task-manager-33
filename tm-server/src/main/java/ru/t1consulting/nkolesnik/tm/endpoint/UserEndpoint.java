package ru.t1consulting.nkolesnik.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IUserEndpoint;
import ru.t1consulting.nkolesnik.tm.api.service.IServiceLocator;
import ru.t1consulting.nkolesnik.tm.api.service.IUserService;
import ru.t1consulting.nkolesnik.tm.dto.request.user.*;
import ru.t1consulting.nkolesnik.tm.dto.response.user.*;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.t1consulting.nkolesnik.tm.api.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserChangePasswordResponse changePassword(
            @WebParam(name = "request", partName = "request")
            @NotNull UserChangePasswordRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable String password = request.getPassword();
        getUserService().setPassword(userId, password);
        return new UserChangePasswordResponse();

    }

    @NotNull
    @Override
    @WebMethod
    public UserLockResponse lockUser(
            @WebParam(name = "request", partName = "request")
            @NotNull UserLockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable String login = request.getLogin();
        getUserService().lockUserByLogin(login);
        return new UserLockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserRegistryResponse registryUser(
            @WebParam(name = "request", partName = "request")
            @NotNull UserRegistryRequest request
    ) {
        @Nullable String login = request.getLogin();
        @Nullable String password = request.getPassword();
        @Nullable String email = request.getEmail();
        getUserService().create(login, password, email);
        return new UserRegistryResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserRemoveResponse removeUser(
            @WebParam(name = "request", partName = "request")
            @NotNull UserRemoveRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable String login = request.getLogin();
        getUserService().removeByLogin(login);
        return new UserRemoveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserUnlockResponse unlockUser(
            @WebParam(name = "request", partName = "request")
            @NotNull UserUnlockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable String login = request.getLogin();
        getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserUpdateResponse updateUser(
            @WebParam(name = "request", partName = "request")
            @NotNull UserUpdateRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable String firstName = request.getFirstName();
        @Nullable String lastName = request.getLastName();
        @Nullable String middleName = request.getMiddleName();
        getUserService().updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse showProfileUser(
            @WebParam(name = "request", partName = "request")
            @NotNull UserProfileRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final User user = getUserService().findById(userId);
        return new UserProfileResponse(user);
    }

}
