package ru.t1consulting.nkolesnik.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.dto.request.task.TaskBindToProjectRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.task.TaskUnbindFromProjectRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.task.TaskBindToProjectResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.task.TaskUnbindFromProjectResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IProjectTaskEndpoint extends IEndpoint{

    @NotNull
    String NAME = "ProjectTaskEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectTaskEndpoint newInstance(){
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectTaskEndpoint newInstance(
            @NotNull final String host,
            @NotNull final String port
    ){
        @NotNull final String wsdl = "http://"+host+":"+port+"/"+NAME+"?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(SPACE, PART);
        return Service.create(url,qName).getPort(IProjectTaskEndpoint.class);
    }

    @NotNull
    @WebMethod
    TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskBindToProjectRequest request
    );

    @NotNull
    @WebMethod
    TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam (name = "request", partName = "request")
            @NotNull TaskUnbindFromProjectRequest request
    );

}
