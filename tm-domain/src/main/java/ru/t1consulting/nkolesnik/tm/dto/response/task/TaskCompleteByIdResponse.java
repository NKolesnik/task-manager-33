package ru.t1consulting.nkolesnik.tm.dto.response.task;

import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.model.Task;

public class TaskCompleteByIdResponse extends AbstractTaskResponse {

    public TaskCompleteByIdResponse(@Nullable Task task) {
        super(task);
    }

}
