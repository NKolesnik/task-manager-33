package ru.t1consulting.nkolesnik.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

public interface IEndpoint {

    @NotNull
    String HOST = "localhost";

    @NotNull
    String PORT = "8080";

    @NotNull
    String SPACE = "http://endpoint.tm.nkolesnik.t1consulting.ru/";

}
