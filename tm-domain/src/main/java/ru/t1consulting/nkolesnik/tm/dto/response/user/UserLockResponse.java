package ru.t1consulting.nkolesnik.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1consulting.nkolesnik.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public final class UserLockResponse extends AbstractResultResponse {

}

