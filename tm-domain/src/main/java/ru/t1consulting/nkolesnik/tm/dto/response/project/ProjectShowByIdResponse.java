package ru.t1consulting.nkolesnik.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.model.Project;

public class ProjectShowByIdResponse extends AbstractProjectResponse {

    public ProjectShowByIdResponse(@Nullable final Project project) {
        super(project);
    }

}
