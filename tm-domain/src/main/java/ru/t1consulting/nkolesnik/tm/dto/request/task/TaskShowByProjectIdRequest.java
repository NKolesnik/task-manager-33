package ru.t1consulting.nkolesnik.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskShowByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskShowByProjectIdRequest(@Nullable final String projectId) {
        this.projectId = projectId;
    }

}
