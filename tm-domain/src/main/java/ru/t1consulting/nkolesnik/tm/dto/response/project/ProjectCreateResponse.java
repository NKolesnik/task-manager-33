package ru.t1consulting.nkolesnik.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.model.Project;

public class ProjectCreateResponse extends AbstractProjectResponse {

    public ProjectCreateResponse(@Nullable final Project project) {
        super(project);
    }

}
