package ru.t1consulting.nkolesnik.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.exception.AbstractException;

public final class CommandNotSupportedException extends AbstractException {

    public CommandNotSupportedException() {
        super("Error! Command not supported...");
    }

    public CommandNotSupportedException(@NotNull final String command) {
        super("Error! Command ``" + command + "`` not supported...");
    }

}
