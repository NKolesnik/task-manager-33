package ru.t1consulting.nkolesnik.tm.client.soap;

import ru.t1consulting.nkolesnik.tm.api.endpoint.ISystemEndpoint;
import ru.t1consulting.nkolesnik.tm.dto.request.system.ServerAboutRequest;

import java.net.MalformedURLException;

public class SystemEndpointClientSoap {

    public static void main(String[] args) throws MalformedURLException {
        System.out.println(ISystemEndpoint.newInstance().getAbout( new ServerAboutRequest()).getEmail());
    }

}
