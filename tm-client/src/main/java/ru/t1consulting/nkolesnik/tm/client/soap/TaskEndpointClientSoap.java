package ru.t1consulting.nkolesnik.tm.client.soap;

import ru.t1consulting.nkolesnik.tm.api.endpoint.ITaskEndpoint;
import ru.t1consulting.nkolesnik.tm.dto.request.task.TaskListRequest;

import java.net.MalformedURLException;

public class TaskEndpointClientSoap {

    public static void main(String[] args) throws MalformedURLException {
        System.out.println(ITaskEndpoint.newInstance().listTask( new TaskListRequest()).getTasks());
    }

}
