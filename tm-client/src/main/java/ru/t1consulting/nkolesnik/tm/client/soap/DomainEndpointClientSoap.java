package ru.t1consulting.nkolesnik.tm.client.soap;

import ru.t1consulting.nkolesnik.tm.api.endpoint.IDomainEndpoint;
import ru.t1consulting.nkolesnik.tm.dto.request.data.DataBackupSaveRequest;

import java.net.MalformedURLException;

public class DomainEndpointClientSoap {

    public static void main(String[] args) throws MalformedURLException {
        System.out.println(IDomainEndpoint.newInstance().saveDataBackup( new DataBackupSaveRequest()));
    }

}
