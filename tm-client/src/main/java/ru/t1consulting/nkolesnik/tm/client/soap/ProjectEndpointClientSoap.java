package ru.t1consulting.nkolesnik.tm.client.soap;

import ru.t1consulting.nkolesnik.tm.api.endpoint.IProjectEndpoint;
import ru.t1consulting.nkolesnik.tm.dto.request.project.ProjectListRequest;

import java.net.MalformedURLException;

public class ProjectEndpointClientSoap {

    public static void main(String[] args) throws MalformedURLException {
        System.out.println(IProjectEndpoint.newInstance().listProject( new ProjectListRequest()).getProjects());
    }

}
