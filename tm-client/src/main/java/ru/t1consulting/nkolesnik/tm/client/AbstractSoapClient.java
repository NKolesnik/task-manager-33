package ru.t1consulting.nkolesnik.tm.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractSoapClient {

    @NotNull
    private String host = "localhost";

    @NotNull
    private String port = "8080";

}
