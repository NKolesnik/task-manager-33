package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.endpoint.*;
import ru.t1consulting.nkolesnik.tm.client.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ITaskEndpointClient getTaskEndpointClient();

    @NotNull
    IProjectEndpointClient getProjectEndpointClient();

    @NotNull
    IProjectTaskEndpointClient getProjectTaskEndpointClient();

    @NotNull
    IUserEndpointClient getUserEndpointClient();

    @NotNull
    IAuthEndpointClient getAuthEndpointClient();

    @NotNull
    ISystemEndpointClient getSystemEndpointClient();

    @NotNull
    IDomainEndpointClient getDomainEndpointClient();

    @NotNull
    ConnectionEndpointClient getConnectionEndpointClient();


}
