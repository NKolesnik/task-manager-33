package ru.t1consulting.nkolesnik.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IDomainEndpoint;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IDomainEndpointClient;
import ru.t1consulting.nkolesnik.tm.dto.request.data.*;
import ru.t1consulting.nkolesnik.tm.dto.response.data.*;

public class DomainEndpointClient extends AbstractEndpointClient implements IDomainEndpointClient {

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupLoadResponse loadDataBackup(@NotNull final DataBackupLoadRequest request) {
        return call(request, DataBackupLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupSaveResponse saveDataBackup(@NotNull final DataBackupSaveRequest request) {
        return call(request, DataBackupSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64LoadResponse loadDataBase64(@NotNull final DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64SaveResponse saveDataBase64(@NotNull final DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinaryLoadResponse loadDataBinary(@NotNull final DataBinaryLoadRequest request) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinarySaveResponse saveDataBinary(@NotNull final DataBinarySaveRequest request) {
        return call(request, DataBinarySaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonFasterXmlLoadResponse loadDataJsonFasterXml(@NotNull DataJsonFasterXmlLoadRequest request) {
        return call(request, DataJsonFasterXmlLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonFasterXmlSaveResponse saveDataJsonFasterXml(@NotNull DataJsonFasterXmlSaveRequest request) {
        return call(request, DataJsonFasterXmlSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonJaxbLoadResponse loadDataJsonJaxb(@NotNull DataJsonJaxbLoadRequest request) {
        return call(request, DataJsonJaxbLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonJaxbSaveResponse saveDataJsonJaxb(@NotNull DataJsonJaxbSaveRequest request) {
        return call(request, DataJsonJaxbSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlFasterXmlLoadResponse loadDataXmlFasterXml(@NotNull DataXmlFasterXmlLoadRequest request) {
        return call(request, DataXmlFasterXmlLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlFasterXmlSaveResponse saveDataXmlFasterXml(@NotNull DataXmlFasterXmlSaveRequest request) {
        return call(request, DataXmlFasterXmlSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlJaxbLoadResponse loadDataXmlJaxb(@NotNull DataXmlJaxbLoadRequest request) {
        return call(request, DataXmlJaxbLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlJaxbSaveResponse saveDataXmlJaxb(@NotNull DataXmlJaxbSaveRequest request) {
        return call(request, DataXmlJaxbSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataYamlFasterXmlLoadResponse loadYamlFasterXml(@NotNull DataYamlFasterXmlLoadRequest request) {
        return call(request, DataYamlFasterXmlLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataYamlFasterXmlSaveResponse saveDataYamlFasterXml(@NotNull DataYamlFasterXmlSaveRequest request) {
        return call(request, DataYamlFasterXmlSaveResponse.class);
    }
}

