package ru.t1consulting.nkolesnik.tm.client.soap;

import ru.t1consulting.nkolesnik.tm.api.endpoint.IProjectTaskEndpoint;
import ru.t1consulting.nkolesnik.tm.dto.request.task.TaskBindToProjectRequest;

import java.net.MalformedURLException;

public class ProjectTaskEndpointClientSoap {

    public static void main(String[] args) throws MalformedURLException {
        System.out.println(IProjectTaskEndpoint.newInstance().bindTaskToProject( new TaskBindToProjectRequest()));
    }

}
