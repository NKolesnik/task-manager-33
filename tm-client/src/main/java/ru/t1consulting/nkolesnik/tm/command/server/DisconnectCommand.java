package ru.t1consulting.nkolesnik.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IEndpointClient;
import ru.t1consulting.nkolesnik.tm.api.service.IServiceLocator;
import ru.t1consulting.nkolesnik.tm.command.AbstractCommand;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;

import java.net.Socket;
import java.util.concurrent.ExecutorService;

public class DisconnectCommand extends AbstractCommand {

    @NotNull
    public static final String NAME = "disconnect";

    @NotNull
    public static final String DESCRIPTION = "Disconnect from server.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }


    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        try {
            serviceLocator.getConnectionEndpointClient().disconnect();
        } catch (@NotNull final Exception e){
            serviceLocator.getLoggerService().error(e);
        }
    }

}
