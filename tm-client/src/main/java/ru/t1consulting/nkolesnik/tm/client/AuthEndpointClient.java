package ru.t1consulting.nkolesnik.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IAuthEndpoint;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IAuthEndpointClient;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserLoginRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserLogoutRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserProfileRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserLoginResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserLogoutResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;

@NoArgsConstructor
public class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    public AuthEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        final AuthEndpointClient client = new AuthEndpointClient();
        client.connect();
        System.out.println(client.login(new UserLoginRequest("admin", "admin")).getSuccess());
        System.out.println(client.profile(new UserProfileRequest()).getUser().getLogin());
        System.out.println(client.profile(new UserProfileRequest()).getUser().getEmail());
        System.out.println(client.profile(new UserProfileRequest()).getUser().getRole());
        System.out.println(client.logout(new UserLogoutRequest()));
        client.disconnect();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserLoginResponse login(
            @WebParam(name = "request", partName = "request")
            @NotNull UserLoginRequest request
    ) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserLogoutResponse logout(
            @WebParam(name = "request", partName = "request")
            @NotNull UserLogoutRequest request
    ) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserProfileResponse profile(
            @WebParam(name = "request", partName = "request")
            @NotNull UserProfileRequest request
    ) {
        return call(request, UserProfileResponse.class);
    }
}
