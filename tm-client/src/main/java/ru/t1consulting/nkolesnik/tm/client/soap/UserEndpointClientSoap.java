package ru.t1consulting.nkolesnik.tm.client.soap;

import ru.t1consulting.nkolesnik.tm.api.endpoint.IUserEndpoint;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserProfileRequest;

import java.net.MalformedURLException;

public class UserEndpointClientSoap {

    public static void main(String[] args) throws MalformedURLException {
        System.out.println(IUserEndpoint.newInstance().showProfileUser(new UserProfileRequest()).getUser());
    }

}
