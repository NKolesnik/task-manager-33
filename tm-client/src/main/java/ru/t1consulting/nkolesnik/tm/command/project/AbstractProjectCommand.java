package ru.t1consulting.nkolesnik.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IProjectEndpoint;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IProjectEndpointClient;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IProjectTaskEndpoint;
import ru.t1consulting.nkolesnik.tm.command.AbstractCommand;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.util.DateUtil;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    public IProjectEndpointClient getProjectEndpointClient() {
        return serviceLocator.getProjectEndpointClient();
    }

    @NotNull
    public IProjectTaskEndpoint getProjectTaskEndpoint() {
        return serviceLocator.getProjectTaskEndpointClient();
    }

    protected void showProject(@Nullable final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(project.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(project.getDateEnd()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
