package ru.t1consulting.nkolesnik.tm.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IEndpointClient;
import ru.t1consulting.nkolesnik.tm.dto.response.ApplicationErrorResponse;

import java.io.*;
import java.net.Socket;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEndpointClient implements IEndpointClient {

    @NotNull
    private String host = "localhost";

    @NotNull
    private Integer port = 6060;

    @Nullable
    private Socket socket;

    public AbstractEndpointClient(@NotNull final AbstractEndpointClient client) {
        this.host = client.host;
        this.port = client.port;
        this.socket = client.socket;
    }

    @Nullable
    @SneakyThrows
    public Socket connect() {
        socket = new Socket(host, port);
        return socket;
    }

    @Nullable
    @SneakyThrows
    public Socket disconnect() {
        if (socket == null) return null;
        socket.close();
        return socket;
    }

    @NotNull
    @SneakyThrows
    protected Object call(@NotNull Object data) {
        getObjectOutputStream().writeObject(data);
        return getObjectInputStream().readObject();
    }

    @NotNull
    protected <T> T call(@NotNull final Object data, Class<T> clazz) throws IOException, ClassNotFoundException {
        getObjectOutputStream().writeObject(data);
        @NotNull final Object result = getObjectInputStream().readObject();
        if (result instanceof ApplicationErrorResponse) {
            @NotNull final ApplicationErrorResponse response = (ApplicationErrorResponse) result;
            throw new RuntimeException(response.getMessage());
        }
        return (T) result;
    }

    @NotNull
    @SneakyThrows
    private ObjectOutputStream getObjectOutputStream() {
        return new ObjectOutputStream(getOutputStream());
    }

    @NotNull
    @SneakyThrows
    private ObjectInputStream getObjectInputStream(){
        return new ObjectInputStream(getInputStream());
    }

    @Nullable
    @SneakyThrows
    private OutputStream getOutputStream() {
        if (socket == null) return null;
        return socket.getOutputStream();
    }

    @Nullable
    @SneakyThrows
    private InputStream getInputStream() {
        if (socket == null) return null;
        return socket.getInputStream();
    }

}
