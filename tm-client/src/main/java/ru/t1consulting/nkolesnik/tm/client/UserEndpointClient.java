package ru.t1consulting.nkolesnik.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IUserEndpoint;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IUserEndpointClient;
import ru.t1consulting.nkolesnik.tm.dto.request.user.*;
import ru.t1consulting.nkolesnik.tm.dto.response.user.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;

@NoArgsConstructor
public class UserEndpointClient extends AbstractEndpointClient implements IUserEndpointClient {

    public UserEndpointClient(@NotNull AbstractEndpointClient abstractClient) {
        super(abstractClient);
    }

    public static void main(String[] args) {
        AuthEndpointClient authClient = new AuthEndpointClient();
        authClient.connect();
        authClient.login(new UserLoginRequest("test", "test"));
        UserEndpointClient userClient = new UserEndpointClient(authClient);
        System.out.println(userClient.showProfileUser(new UserProfileRequest()).getUser().getLogin());
        System.out.println(authClient.logout(new UserLogoutRequest()));
        authClient.disconnect();
    }

    @WebMethod
    @NotNull
    @Override
    @SneakyThrows
    public UserRegistryResponse registryUser(@WebParam(name = "request", partName = "request") @NotNull final UserRegistryRequest request) {
        return call(request, UserRegistryResponse.class);
    }

    @WebMethod
    @NotNull
    @Override
    @SneakyThrows
    public UserRemoveResponse removeUser(@WebParam(name = "request", partName = "request") @NotNull final UserRemoveRequest request) {
        return call(request, UserRemoveResponse.class);
    }

    @WebMethod
    @NotNull
    @Override
    @SneakyThrows
    public UserUpdateResponse updateUser(@WebParam(name = "request", partName = "request") @NotNull final UserUpdateRequest request) {
        return call(request, UserUpdateResponse.class);
    }

    @WebMethod
    @NotNull
    @Override
    @SneakyThrows
    public UserProfileResponse showProfileUser(@WebParam(name = "request", partName = "request") @NotNull final UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

    @WebMethod
    @NotNull
    @Override
    @SneakyThrows
    public UserChangePasswordResponse changePassword(@WebParam(name = "request", partName = "request") @NotNull final UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

    @WebMethod
    @NotNull
    @Override
    @SneakyThrows
    public UserLockResponse lockUser(@WebParam(name = "request", partName = "request") @NotNull final UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @WebMethod
    @NotNull
    @Override
    @SneakyThrows
    public UserUnlockResponse unlockUser(@WebParam(name = "request", partName = "request") @NotNull final UserUnlockRequest request) {
        return call(request, UserUnlockResponse.class);
    }

}

